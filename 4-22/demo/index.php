<?php 

// 引入自定义的模板
include './Xbs/Libs/function.php';

// 定义配置文件路径常量
define('CONFIG_PATH', './Config/config.php');

// 自动加载类
function __autoload($classname)
{
	// 获取自动加载目录
	$class_path = C('CLASS_PATH');
	// 循环组合类文件并判断是否存在，存在则引入
	foreach ($class_path as $path) {
		$path = "./".$path."/".$classname.".class.php";
		if(file_exists($path)){
			include $path;
			break;
		}
	}
}

// 获取路径中的参数，C为控制器，a为方法
$class = isset($_GET['c']) ? ucfirst($_GET['c']) :'Index';
$action = isset($_GET['a']) ? ucfirst($_GET['a']) :'Index';

// 定义控制器常量，后面用
define('CURRENT_CONTROLLER', $class);
// 定义方法常量，后面用
define('CURRENT_ACTION', $action);

// 组控制器名
$classname = $class."Controller";

// 实例化控制器，产生对象
$obj = new $classname();
//执行对象里面的方法
$obj->$action();













 ?>
mysql> select * from stu t1 left join electives t2 on t1.id=t2.uid;
+----+--------+----------------------------------+-----+------+------+
| id | name   | password                         | cid | uid  | coid |
+----+--------+----------------------------------+-----+------+------+
|  1 | 曾九峰 | 4bec52e77ce530aa470e302ef2d52556 |   2 |    1 |    3 |
|  1 | 曾九峰 | 4bec52e77ce530aa470e302ef2d52556 |   2 |    1 |    4 |
|  1 | 曾九峰 | 4bec52e77ce530aa470e302ef2d52556 |   2 |    1 |    2 |
|  2 | 林天福 | 4bec52e77ce530aa470e302ef2d52556 |   1 |    2 |    1 |
|  3 | 杨春辉 | 4bec52e77ce530aa470e302ef2d52556 |   1 |    3 |    2 |
|  5 | 曾建平 | 4bec52e77ce530aa470e302ef2d52556 |   2 |    5 |    2 |
|  4 | 郭海鹏 | 4bec52e77ce530aa470e302ef2d52556 |   1 |    4 |    3 |
|  4 | 郭海鹏 | 4bec52e77ce530aa470e302ef2d52556 |   1 |    4 |    2 |
|  3 | 杨春辉 | 4bec52e77ce530aa470e302ef2d52556 |   1 |    3 |    3 |
|  2 | 林天福 | 4bec52e77ce530aa470e302ef2d52556 |   1 |    2 |    4 |
|  6 | 马志斌 | 4bec52e77ce530aa470e302ef2d52556 |   5 | NULL | NULL |
+----+--------+----------------------------------+-----+------+------+
11 rows in set (0.00 sec)

mysql> select * from stu t1 left join electives t2 on t1.id=t2.uid where t1.id=2;
+----+--------+----------------------------------+-----+------+------+
| id | name   | password                         | cid | uid  | coid |
+----+--------+----------------------------------+-----+------+------+
|  2 | 林天福 | 4bec52e77ce530aa470e302ef2d52556 |   1 |    2 |    1 |
|  2 | 林天福 | 4bec52e77ce530aa470e302ef2d52556 |   1 |    2 |    4 |
+----+--------+----------------------------------+-----+------+------+
2 rows in set (0.00 sec)

mysql> select * from stu t1 left join electives t2 on t1.id=t2.uid left join course t3 on t2.coid=t3.id where t1.id=2;
+----+--------+----------------------------------+-----+------+------+------+------------+-------+
| id | name   | password                         | cid | uid  | coid | id   | coursename | score |
+----+--------+----------------------------------+-----+------+------+------+------------+-------+
|  2 | 林天福 | 4bec52e77ce530aa470e302ef2d52556 |   1 |    2 |    1 |    1 | PS         | 3     |
|  2 | 林天福 | 4bec52e77ce530aa470e302ef2d52556 |   1 |    2 |    4 |    4 | JS         | 4     |
+----+--------+----------------------------------+-----+------+------+------+------------+-------+
2 rows in set (0.00 sec)

mysql> select t1.name,t3.coursename from stu t1 left join electives t2 on t1.id=t2.uid left join course t3 on t2.coid=t3.id where t1.id=2;
+--------+------------+
| name   | coursename |
+--------+------------+
| 林天福 | PS         |
| 林天福 | JS         |
+--------+------------+
2 rows in set (0.00 sec)

mysql> select t1.name,t3.coursename from stu t1 left join electives t2 on t1.id=t2.uid left join course t3 on t2.coid=t3.id where t1.id=1;
+--------+------------+
| name   | coursename |
+--------+------------+
| 曾九峰 | HTML       |
| 曾九峰 | PHP        |
| 曾九峰 | JS         |
+--------+------------+
3 rows in set (0.00 sec)

mysql> select t1.name,t3.coursename from stu t1 left join electives t2 on t1.id=t2.uid left join course t3 on t2.coid=t3.id where t1.id=3;
+--------+------------+
| name   | coursename |
+--------+------------+
| 杨春辉 | HTML       |
| 杨春辉 | PHP        |
+--------+------------+
2 rows in set (0.00 sec)

mysql> select t1.name,t3.coursename from stu t1 left join electives t2 on t1.id=t2.uid left join course t3 on t2.coid=t3.id where t1.id=4;
+--------+------------+
| name   | coursename |
+--------+------------+
| 郭海鹏 | HTML       |
| 郭海鹏 | PHP        |
+--------+------------+
2 rows in set (0.00 sec)

mysql> select t1.name,t3.coursename from stu t1 left join electives t2 on t1.id=t2.uid left join course t3 on t2.coid=t3.id where t1.id=5;
+--------+------------+
| name   | coursename |
+--------+------------+
| 曾建平 | HTML       |
+--------+------------+
1 row in set (0.00 sec)

mysql> select t1.name,t3.coursename from stu t1 left join electives t2 on t1.id=t2.uid left join course t3 on t2.coid=t3.id where t1.id=6;
+--------+------------+
| name   | coursename |
+--------+------------+
| 马志斌 | NULL       |
+--------+------------+
1 row in set (0.00 sec)

mysql> select * from stu,electives,course where stu.id=electives.uid and electives.coid=
    ->
    -> course.id and stu.id=2
    -> ;
+----+--------+----------------------------------+-----+-----+------+----+------------+-------+
| id | name   | password                         | cid | uid | coid | id | coursename | score |
+----+--------+----------------------------------+-----+-----+------+----+------------+-------+
|  2 | 林天福 | 4bec52e77ce530aa470e302ef2d52556 |   1 |   2 |    1 |  1 | PS         | 3     |
|  2 | 林天福 | 4bec52e77ce530aa470e302ef2d52556 |   1 |   2 |    4 |  4 | JS         | 4     |
+----+--------+----------------------------------+-----+-----+------+----+------------+-------+
2 rows in set (0.00 sec)

mysql>

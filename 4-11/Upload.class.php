<?php 

/**
* 自己设计的上传类
*/
class Upload 
{
	public $path;//上传目录
	public $size;//限制上传字节
	public $type;//限制上传类型
	public $name;//生成上传文件名
	public $uploadData; //上传的键名
	public $key; //上传的键名
	// 构造函数 
	function __construct($config=array())
	{
		$this->path = isset($config['path']) ? $config['path'] : C('UPLOAD_PATH');
		$this->size = isset($config['size']) ? $config['size'] : C('UPLOAD_SIZE');
		$this->type = isset($config['type']) ? $config['type'] : C('UPLOAD_TYPE');
		$this->name = isset($config['name']) ? $config['name'] : C('UPLOAD_NAME');
	}
	/**
	 * 上传保存图片
	 */
	public function save(){
		$this->uploadData=$_FILES;
		$this->key=key($_FILES);
		$this->_check();
		$this->_move();
	}

	/**
	 * 检测图片是否条例要求
	 */

	private  function _check(){
		if(!is_array($this->uploadData[$this->key]['tmp_name'])){
			$this->_size($this->uploadData[$this->key]['size']);
			$this->_type($this->uploadData[$this->key]['name']);
		}else{
			for ($i=0; $i <count($this->uploadData[$this->key]['tmp_name']) ; $i++) { 
				$this->_size($this->uploadData[$this->key]['size'][$i]);
				$this->_type($this->uploadData[$this->key]['name'][$i]);
			}
		}

	}

	//判断大小
	public function _size($size){
		if($size>$this->size){
			echo "大小超过限制，请重新处理";
			exit;
		}
	}

	//检测类型
	public function _type($type){
		$type = strrchr($type, '.');

		if(!in_array($type, $this->type)){
			$str = implode(',', $this->type);
			echo "文件类型不对，请上传$str";
			exit;
		}
		
	}
	/**
	 * 移动图片到指定位置
	 */
	private  function _move(){
		if(!is_array($this->uploadData[$this->key]['tmp_name'])){
			$this->_moveflie($this->uploadData[$this->key]['tmp_name']);
		}else{
			for ($i=0; $i <count($this->uploadData[$this->key]['tmp_name']) ; $i++) { 
				$this->_moveflie($this->uploadData[$this->key]['tmp_name'][$i]);
			}
		}
	}

	//移动文件
	private  function _moveflie($tmpPath){
		if(is_uploaded_file($tmpPath)){
			$dir = $this->path;

			!is_dir($dir)? mkdir($dir,777,true) : '' ;

			$path = $dir.$this->name;

			move_uploaded_file($tmpPath,$path);
		}
	}


}




 ?>